import { LOAD_AUTHOR_SUCCESS } from "../actions/actionsTypes";
import initialState from "./initialState";

export default function authorReducer(state = initialState.authors, action) {
  switch (action.type) {
    case LOAD_AUTHOR_SUCCESS:
      return action.authors;
    default:
      return state;
  }
}

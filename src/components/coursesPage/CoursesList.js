import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const CoursesList = ({ courses, onDeleteClick }) => (
  <table className="table">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <th>Title</th>
        <th>Author ID</th>
        <th>Category</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {courses.length > 0 &&
        courses.map(course => {
          return (
            <tr key={course.id}>
              <td>
                <a
                  className="btn btn-light"
                  // onClick={() => deleteCourse(course.id)}
                  href={`http://pluralsight.com/courses/${course.slug}`}
                >
                  Watch
                </a>
              </td>
              <td>
                <Link to={`/course/${course.slug}`}>{course.title}</Link>
              </td>
              <td>{course.authorName}</td>
              <td>{course.category}</td>
              <td>
                <button
                  className='btn btn-outline-danger'
                  onClick={() => onDeleteClick(course)}>
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
    </tbody>
  </table>
);

CoursesList.propTypes = {
  // courses: PropTypes.array.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
  courses: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      authorId: PropTypes.number.isRequired,
      category: PropTypes.string.isRequired
    })
  ).isRequired
};

CoursesList.defaultProps = {
  courses: []
};

export default CoursesList;

import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
  const activeStyle = {
    color: "#F15B2A"
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <NavLink className="navbar-brand" exact to="/">
        <img
          src="/Images/NLUnactiveImg.png"
          alt="logo"
        />
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink
            className="nav-item nav-link"
            activeStyle={activeStyle}
            to="/"
            exact
          >
            Inicio
          </NavLink>
          <NavLink
            className="nav-item nav-link"
            activeStyle={activeStyle}
            to="/courses"
          >
            Courses
          </NavLink>
          <NavLink
            className="nav-item nav-link"
            activeStyle={activeStyle}
            to="/about"
          >
            About
          </NavLink>
        </div>
      </div>
    </nav>
  );
}

export default Header;

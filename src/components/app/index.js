import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "../homePage";
import CoursesPage from "../coursesPage";
import AboutPage from "../aboutPage";
import Header from "../common/Header";
import NotFoundPage from "../pageNotFound";
import ManageCoursePage from "../coursesPage/ManageCoursePage";

function App() {
  return (
    <div className="container">
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/courses" component={CoursesPage} />
        <Route path="/course/:slug" component={ManageCoursePage} />
        <Route path="/course" component={ManageCoursePage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

export default App;

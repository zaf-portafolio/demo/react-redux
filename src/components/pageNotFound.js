import React from "react";
import { Link } from "react-router-dom";

function NotFoundPage() {
  return (
    <div>
      <h1> PAGE NOT FOUND</h1>
      <p>
        <Link to="/"> Back to Home </Link>
      </p>
    </div>
  );
}

export default NotFoundPage;
